<?php
ini_set('display_errors', 1);
/**
 * Instagram PHP API
 *
 * @link https://github.com/cosenary/Instagram-PHP-API
 * @author Christian Metz
 * @since 01.10.2013
 */
require '../src/Instagram.php';

use MetzWeb\Instagram\Instagram;

// initialize class
$instagram = new Instagram(array(
    'apiKey' => 'a77e09905fca4bf8bf7ceb3a990d6044',
    'apiSecret' => 'ebd457fe3cee480caa53945b1a1f06d4',
    'apiCallback' => 'http://dev.trip.com.br/scripts/Instagram-PHP-API/app/success.php' // must point to success.php
));

// receive OAuth code parameter
$code = $_GET['code'];

// check whether the user has granted access
if (isset($code)) {
    // receive OAuth token object
    $data = $instagram->getOAuthToken($code);
    $username = $data->user->username;
    // store user access token
    $instagram->setAccessToken($data);
    // now you have access to all authenticated user methods
    //$result = $instagram->getUserMedia();
    $result = $instagram->getUserMedia('self',40);
    //var_dump($result);
    //die();

    //var_dump($result->data[0]->created_time);
    $returnData = $result->data;
    $resultDataCount = sizeof($result->data);

    function returnCount($object){

        return $object->count;

    }

    function returnCaptionText($object){
        return $object->text;
    }


    for ($i=0; $i < $resultDataCount ; $i++) { 
        $formatedResult[$i]['gatheringdate']    =   date('Y-m-d H:i:s');
        $formatedResult[$i]['postdate']         = date('Y-m-d H:i:s',$returnData[$i]->created_time);
        $formatedResult[$i]['posturl']          = $returnData[$i]->link;
        $formatedResult[$i]['posttype']         = $returnData[$i]->type;
        $formatedResult[$i]['commentscount']    = returnCount($returnData[$i]->comments); 
        $formatedResult[$i]['likescount']       = returnCount($returnData[$i]->likes); 
        $formatedResult[$i]['postid']           = $returnData[$i]->id;
        $formatedResult[$i]['postcaption']      = returnCaptionText($returnData[$i]->caption);
    }

    //var_dump($formatedResult);

    $resultFollowers = $instagram->getUser();

    var_dump($resultFollowers);



} else {
    // check whether an error occurred
    if (isset($_GET['error'])) {
        echo 'An error occurred: ' . $_GET['error_description'];
    }
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Instagram - photo stream</title>
    <link href="https://vjs.zencdn.net/4.2/video-js.css" rel="stylesheet">
    <link href="assets/style.css" rel="stylesheet">
    <script src="https://vjs.zencdn.net/4.2/video.js"></script>
</head>
<body>
<div class="container">
    <header class="clearfix">
        <img src="assets/instagram.png" alt="Instagram logo">

        <h1>Instagram photos <span>taken by <?php echo $data->user->username ?></span></h1>
    </header>
    <div class="main">

    </div>
</div>
<!-- javascript -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

</body>
</html>
